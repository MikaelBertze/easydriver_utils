#include "easydriver_utils.h"
#include <TimerOne.h>

EasyDriver_utils* EasyDriver_utils::_instance;

EasyDriver_utils::EasyDriver_utils() {
  
}

bool EasyDriver_utils::addMotor(motor_def motor) {
  if (_num_motors < 4) {
    _motors[_num_motors] = motor;
    pin_setup(_num_motors);
    _num_motors++;
    return true;
  }
  return false;
}

void EasyDriver_utils::enable(bool value) {
  for (byte i = 0; i < _num_motors; i++){
    digitalWrite(_motors[i].EN, value ? LOW : HIGH);
  }
}

void EasyDriver_utils::step_one(bool step[]) {
  for (byte i = 0; i < _num_motors; i++){
    digitalWrite(_motors[i].STP, step[i] ? HIGH : LOW);
  }
  delayMicroseconds(1);
  for (byte i = 0; i < _num_motors; i++){
    digitalWrite(_motors[i].STP, LOW);
  }
}

void EasyDriver_utils::direction(bool cw[]) {
 for (byte i = 0; i < _num_motors; i++){
    digitalWrite(_motors[i].DIR, cw[i] ? HIGH : LOW);
  }
}

void EasyDriver_utils::setStepMode(byte mode) {
  for (byte i = 0; i < _num_motors; i++) {
    switch(mode) {
      case 0:
        _steps_per_revolution = 200;
        digitalWrite(_motors[i].MS1, LOW);
        digitalWrite(_motors[i].MS2, LOW);
        break;
      case 1:
        _steps_per_revolution = 400;
        digitalWrite(_motors[i].MS1, HIGH);
        digitalWrite(_motors[i].MS2, LOW);
        break;
      case 2:
        _steps_per_revolution = 800;
        digitalWrite(_motors[i].MS1, LOW);
        digitalWrite(_motors[i].MS2, HIGH);
        break;
      case 3:
        _steps_per_revolution = 1600;
        digitalWrite(_motors[i].MS1, HIGH);
        digitalWrite(_motors[i].MS2, HIGH);
        break;
    }
  }
}

void EasyDriver_utils::stepsPerRevolution() {
  return _steps_per_revolution;
}

void EasyDriver_utils::start() {
  EasyDriver_utils::_instance = this;
  Timer1.initialize();
  Timer1.attachInterrupt(EasyDriver_utils::_doStep, 1000);
  setStepMode(_step_mode);
  enable(true);
}

void EasyDriver_utils::stop() {
  Timer1.stop();
  enable(false);
}
void EasyDriver_utils::setSpeed(double speed){
  bool directions[] = {speed < 0, speed > 0 };
  direction(directions);

  _direction = speed > 0 ? 1 : -1;
  speed *= speed < 0 ? -1.0 : 1.0;
  if (speed <.005) {
    _stepdelay = 0;
    return;
  }

  if (speed > 2.5)
    speed = 2.5;
  

  _stepdelay = 1.0 / speed / _steps_per_revolution * 1e6;
}

void EasyDriver_utils::_step() {
  if (_stepdelay == 0) {
    Timer1.setPeriod(100);
  }
  if (_stepdelay != 0) {
    //Serial.println(_speed);
    Timer1.setPeriod(_stepdelay);
    bool a[] =  {true, true};
    step_one(a);
    // if speed changed, set a new timer perioddetachInterrupt
    _stepCount += _direction;
  }
}

void EasyDriver_utils::printDebug(){
  String s = "";
  Serial.println("--- EASY DRIVER UTILS ---");
  Serial.print("Number of motors: ");
  Serial.println(_num_motors, DEC);
  Serial.print("Step mode: ");
  Serial.println(_step_mode, DEC);
  Serial.print("Min delay: ");
  Serial.println(_min_delay, DEC);
  Serial.print("Max delay: ");
  Serial.println(_max_delay, DEC);
  
  
  s += "\n";
  
  for (byte i = 0; i < _num_motors; i++) {
    motor_def motor = _motors[i];
    Serial.print("MOTOR ");
    Serial.println(i + 1 , DEC);
    Serial.println("-------------------");
    Serial.print("STP: ");
    Serial.println(motor.STP, DEC);
    Serial.print("DIR: ");
    Serial.println(motor.DIR, DEC);
    Serial.print("MS1: ");
    Serial.println(motor.MS1, DEC);
    Serial.print("MS2: ");
    Serial.println(motor.MS2, DEC);
    Serial.print("EN:  ");
    Serial.println(motor.EN, DEC);
    Serial.println("");
  }
}



void EasyDriver_utils::pin_setup(byte motor_index){
  pinMode(_motors[motor_index].STP, OUTPUT);
  pinMode(_motors[motor_index].DIR, OUTPUT);
  pinMode(_motors[motor_index].MS1, OUTPUT);
  pinMode(_motors[motor_index].MS2, OUTPUT);
  pinMode(_motors[motor_index].EN, OUTPUT);
  digitalWrite(_motors[motor_index].STP, LOW);
  digitalWrite(_motors[motor_index].DIR, LOW);
  digitalWrite(_motors[motor_index].MS1, LOW);
  digitalWrite(_motors[motor_index].MS2, LOW);
  digitalWrite(_motors[motor_index].EN, HIGH);
}