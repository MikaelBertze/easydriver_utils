/*
  EasyDriver_utils.h - Library for controlling the easy driver board.
  Created by Mikael Bertze, 2017.
  Released into the public domain.
*/

#ifndef EasyDriver_utils_h
#define EasyDriver_utils_h

#include "Arduino.h"

struct motor_def {
  byte STP;
  byte DIR;
  byte MS1;
  byte MS2;
  byte EN;
};

class EasyDriver_utils
{
  public:
    EasyDriver_utils();
    bool addMotor(motor_def motor);
    void printDebug();
    void enable(bool value);
    void step_one(bool steps[]);
    void direction(bool cw[]);
    void setStepMode(byte mode);
    void stepsPerRevolution();
    void start();
    void stop();
    void setSpeed(double speed);
    void setSpeedParams(int min_delay, int max_delay) {
      _min_delay = min_delay;
      _max_delay = max_delay;
    }
    int StepCount(bool reset) {
      int sc = _stepCount;
      if (reset)
        _stepCount = 0;
      return sc;
    }

  private:    
    void pin_setup(byte motor_index);
    void _step();
    byte _num_motors = 0;
    byte _step_mode = 3;
    motor_def _motors[4];

    unsigned long _stepdelay = 0;
    unsigned long _min_delay = 220;
    unsigned long _max_delay = 4000;
    int _steps_per_revolution = -1;
    int _stepCount = 0;
    int _direction = 0;
    static EasyDriver_utils *_instance;
    
    static void _doStep() {
      _instance->_step();
    }
};

static EasyDriver_utils *_instance;

//EasyDriver_utils EasyDriver_utils::_instance = NULL;

// void _doStepfdfdf() {
//    _instance->step();
//  }

#endif
